#pragma strict

var isGamePause:boolean;
isGamePause = false;

var kiteController:GameObject;
var originalColor:Color;
originalColor = guiTexture.color;
function Start () {

}

function Update () {

}

function OnMouseDown () {
	if(!isGamePause){
		Time.timeScale = 0.0;
		isGamePause=true;
		kiteController.SendMessage('showGameOverLabel',true);
	}else{
		Time.timeScale = 1.0;
		isGamePause=false;
		kiteController.SendMessage('removeGameOverLabel');
	}
}

function OnMouseOver () {
	guiTexture.color = Color(0.2, 0.3, 0.4, 0.2);
}

function OnMouseExit () {
        guiTexture.color = originalColor;
}