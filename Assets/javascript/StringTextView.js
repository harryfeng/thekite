#pragma strict

var pasttime: float;

function Start() {
    guiText.material.color = Color.black;
    //	  Debug.Log(guiText.fontSize);
    guiText.fontSize = 26;
    //	  guiText.text = 20;
}

function Update() {
    if (GameObject.FindGameObjectWithTag('kite')) {
        if (guiText.text != GameObject.FindGameObjectWithTag('kite').GetComponent(KiteModel).ActuralStringNumber.ToString()) {
            guiText.fontSize = 30;
            guiText.material.color = Color.red;
            guiText.transform.position.y = 0.965;
            pasttime = 0;
        } else {
            pasttime += Time.deltaTime;
            if (pasttime > 0.5) {
                guiText.fontSize = 26;
                guiText.material.color = Color.black;
                guiText.transform.position.y = 0.965;
            }
        }
        var stringNumber = GameObject.FindGameObjectWithTag('kite').GetComponent(KiteModel).ActuralStringNumber;
        if(stringNumber>=0)guiText.text = GameObject.FindGameObjectWithTag('kite').GetComponent(KiteModel).ActuralStringNumber.ToString();
    }
}


function OnGUI() {
    GUI.contentColor = Color.yellow;
}