#pragma strict
var birdModel: BirdModel;
birdModel = gameObject.GetComponent(BirdModel);	

var commonMethod:CommonMethod;
commonMethod=FindObjectOfType(CommonMethod);
var timer:float;
timer=0;
var level:int;
var randomSpeed:float;
function Start () {
	level = commonMethod.gerModelVariable('mainGame','MainGameModel').gameLevel;
	randomSpeed = Random.Range(-2.0,2.0);
}

function Update () {
	//transform.Translate(Vector3.right * Time.deltaTime*birdModel.birdSpeed);
	destroyWhenReachEnd(gameObject.transform.localPosition.x);
	vibrateBird(randomSpeed);
	
}

//the bird will vibrate up and down
function vibrateBird(speed:float){
	this.rigidbody.velocity.x = -1;
	timer=timer + Time.deltaTime;
	if(timer >3&&timer<6){
	this.rigidbody.velocity.y = speed;
	}
	else if(timer>6&&timer<9){
		this.rigidbody.velocity.y = -speed;
	}
	else if(timer>9){
		this.rigidbody.velocity.y = 0;
		timer=0;
	}
}

function OnTriggerEnter (other : Collider) {
	if(other.tag=='kite') {
		birdModel.ishit=true;
	}
}

//para:location is the local position of the bird relative to the background 
function destroyWhenReachEnd(location:float){
	if(location>10){
		Destroy (gameObject);
	}
}

