#pragma strict

function Start () {

}

function Update () {
	cloudAction();
}

function OnTriggerEnter (other : Collider) {	
	if(isKiteInCloud(other)) {
		if (Application.platform == RuntimePlatform.IPhonePlayer){
			//Destroy (gameObject);
			this.transform.parent = other.transform;
	    }
	    else{
	    	this.transform.parent = other.transform;
	    }	
	}
}

//check if the kite is in the cloud
//return true if the kite is in the cloud
function isKiteInCloud(gameObj:Collider){
	if(gameObj.tag=='kite'){
		return true;
	}
	else{
		return false;
	}
}

private var ray : Ray;
private var hit : RaycastHit;
//stick with the cloud and you need to swipe it
function cloudAction(){
	transform.Translate(Vector3.right*Time.deltaTime*0.5);
	getTouchObject();
}

//return the game object that touch in iphone
private var timecounter:float;
timecounter=0;
function getTouchObject(){
	if(Input.touchCount == 1){		
		ray = Camera.main.ScreenPointToRay(Input.touches[0].position);
		if(Physics.Raycast(ray.origin, ray.direction * 10,hit) && hit.transform == gameObject.transform.parent&&gameObject.transform.parent.name=='KITE'){
	    	//Debug.Log(Input.GetTouch(0).deltaTime);
	    	timecounter +=Input.GetTouch(0).deltaTime;
	    	if(timecounter>0.05){
	    		Destroy(gameObject);
	    	}
	    }
    }
}

//if kite is escaping from cloud, descrease its opacity
//return opacity of the cloud
function decreaseCloudOpacity(){
	
}

//if the cloud opacity is less than 0.1, decrease the health of the cloud
function reduceCloudHealth(){}

//if the cloud have 0 health, destory the cloud
function destoryCloud(){}

