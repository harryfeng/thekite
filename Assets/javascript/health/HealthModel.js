#pragma strict
var healthNumber:int;
var kiteModel: KiteModel;
var individualHealth:GameObject;
var localposition:Vector3;
localposition = individualHealth.transform.localPosition;
var mainCamera:GameObject;
var individualHealthArray:GameObject[];

function Start () {
	createHealth(kiteModel.kiteHealth);
	individualHealthArray = GameObject.FindGameObjectsWithTag('individualHealth');
	
}

function Update () {
	if(kiteModel){
		healthNumber=kiteModel.kiteHealth;
		changeHealthColor();
	}
}

function createHealth(healthNumber:float){
	if(individualHealth){
		for(var x = 0; x<healthNumber;x++){
		 	var clone:GameObject = Instantiate (individualHealth,localposition,Quaternion.Euler(90,180,0));
		 	clone.transform.parent = gameObject.transform;
		 	clone.transform.localPosition.x = localposition.x + x*17;
		 }
	}
};

function changeHealthColor(){
	if(healthNumber<individualHealthArray.Length&&healthNumber>=0)
	{	
		var health:individualHealth = individualHealthArray[kiteModel.kiteHealth].GetComponent('individualHealth');
		health.isGoodHealth = false;
	}
}