#pragma strict

var kiteModel: KiteModel;
kiteModel = gameObject.GetComponent(KiteModel);	

var commonMethod:CommonMethod;
commonMethod=FindObjectOfType(CommonMethod);

var gameOver:Transform;
var gamePause:Transform;
var gameRestart:Transform;
var gameQuit:Transform;
var oldPosition:Vector3;
var globalObject:globalObject;
globalObject = GameObject.FindGameObjectWithTag('globalObject').GetComponent('globalObject');

oldPosition = this.transform.position;

function Start () {
	Time.timeScale = 1.0;
}

function Update () {
    if (Application.platform == RuntimePlatform.IPhonePlayer||Application.platform==RuntimePlatform.Android){
    	//applyIphoneControl(kiteModel.kiteSpeed);
    	applyIphoneSwipeControl(kiteModel.kiteSpeed);
    }
    else{
    	//applyDesktopControl(kiteModel.kiteSpeed);
    	if(globalObject.kiteHealth==3){
    		Debug.Log('in update');
    		applyDesktopControl(8);
    	}else{
    		applyDesktopForceControl(kiteModel.kiteSpeed);
    	}
    	
    }
	kiteOutofBound();
	showGameOverScreen();
}

//This message is sent to the trigger collider and the rigidbody (or the collider if there is no rigidbody) that touches the trigger. 
//Note that trigger events are only sent if one of the colliders also has a rigid body attached.
function OnTriggerEnter (other : Collider) {
	if(other.tag=='bird'){
		kiteModel.kiteHealth--;
	}
	if(other.tag=='dandelion'){
		kiteModel.stringNumber+=2;
	}
	if(other.tag=='cloud'){
		kiteModel.stringNumber--;
	}
}

//when kite collide with other object, get the tag name of the other collision obj. 
//return the name of the tag name
function getCollistionObjTag(gameObj:GameObject){
	
	return gameObj.tag;
}

//if the kite hit the bird, it will reduce health just once
function reduceHealth(){
	if(kiteModel.isHit) kiteModel.kiteHealth--;
	kiteModel.isHit=false;
	
}

//if the kite hit the dandelion, it will add some string
function addString(){
	kiteModel.stringNumber++;
}

//if the kite hit the cloud, it will stick with the cloud
//return true if it stick with the cloud.
function stickWithCloud(){}

//if the kite stick with the cloud, show help message
function showHelpMessage(){}

//iphone escape from the kite control
function escapeFromCloudIphone(){}

//deskto escape from the kite control
function escapeFromcloudDesktop(){}

//implement the iphone control of the kite
function kiteIphoneControl(){}

//if the kite is out of uppper bound, it will appear at the bottom, if the kite is out of the lower bound, it will appear at the top
function kiteOutofBound(){
	var verticalPosition = gameObject.transform.localPosition.y;
	if(verticalPosition>7){gameObject.transform.localPosition.y=-7;}
	if(verticalPosition<-7){gameObject.transform.localPosition.y=7;}
}

//implement the pc control of the kite
//para: upKey, the key represent up movement, ect. moveSpeed is a moving speed
function applyDesktopControl(moveSpeed:float){
	
	var gameOverLabel = GameObject.FindGameObjectsWithTag('gameOverLabel');
	Debug.Log('outside if');
	if(gameOverLabel.Length==0&&transform.childCount==1){
		Debug.Log('applyDesktopControl');
		var level = commonMethod.gerModelVariable('mainGame','MainGameModel').gameLevel;
		transform.Translate(moveSpeed*-1*Time.deltaTime*0.3*(1),0,0);
		this.transform.position.z = oldPosition.z; 
		if (Input.GetKey ("d"))
		{
			Debug.Log('press d');
			transform.Translate(moveSpeed*-1*Time.deltaTime,0,0); 
		}
		if (Input.GetKey ("a"))
		{
			transform.Translate(0.2*moveSpeed*Time.deltaTime,0,0);
		}
		if (Input.GetKey ("s"))
		{
			transform.Translate(0,0,moveSpeed*Time.deltaTime);
		}
		if (Input.GetKey ("w"))
		{
			transform.Translate(0,0,moveSpeed*-1*Time.deltaTime);
		}
		}
}

private var origiVelocity:Vector3;

function applyDesktopForceControl(moveSpeed:float){
	
	var gameOverLabel = GameObject.FindGameObjectsWithTag('gameOverLabel');	
	if(gameOverLabel.Length==0&&transform.childCount==1){
		origiVelocity = this.rigidbody.velocity;
		var level = commonMethod.gerModelVariable('mainGame','MainGameModel').gameLevel;		
		this.transform.position.z = oldPosition.z;
		if (Input.GetKey ("d"))
		{
			this.rigidbody.AddForce (Vector3.right * moveSpeed);
		}
		if (Input.GetKey ("a"))
		{
			if(this.rigidbody.velocity.x>0)this.rigidbody.AddForce (Vector3.left * moveSpeed);
		}
		if (Input.GetKey ("s"))
		{
			this.rigidbody.AddForce (Vector3.down * moveSpeed);
		}
		if (Input.GetKey ("w"))
		{
			this.rigidbody.AddForce (Vector3.up * moveSpeed);
		}
	}else{
		this.rigidbody.velocity = origiVelocity/4;
	}
};

function kiteShaking(){
	
}

function applyIphoneControl(moveSpeed:float){
		this.transform.position.z = oldPosition.z; 
		transform.Translate(moveSpeed*-2*Time.deltaTime*0.3*(1),0,0);
		transform.Translate(0,0,moveSpeed*Time.deltaTime);
		  if(Input.anyKey){
		  	transform.Translate(0,0,moveSpeed*-2*Time.deltaTime);
		  }
}

//if the kite health is less than 1, move to game over screen
function showGameOverScreen(){
	if(kiteModel.kiteHealth<1){	
	 renderer.enabled = false; 	
	 gameObject.SendMessage ("showGameOverLabel",false); 
		//Application.LoadLevel (0);
	}
}


var gamePauseTexture:Texture;
var originalGameTexture:Texture;
originalGameTexture = gameObject.renderer.material.mainTexture;

function showGameOverLabel(isPause){
	var gameOverLabel = GameObject.FindGameObjectsWithTag('gameOverLabel');
	var mainCamera = GameObject.FindGameObjectWithTag('MainCamera');
	if(gameOverLabel.Length<3){
		Debug.Log(isPause);
		if(isPause){
			Debug.Log('in the pause');
			//if(gamePauseTexture)gameOver.renderer.material.mainTexture = gamePauseTexture;
			var clonePause = Instantiate(gamePause);
			clonePause.parent=mainCamera.transform;
 		 	clonePause.localPosition = Vector3(0,0,18);
		}else{
			var clone1 = Instantiate(gameOver);
		 	clone1.parent=mainCamera.transform;
 		 	clone1.localPosition = Vector3(0,0,18);


		}
		
		var clone2 = Instantiate(gameRestart);
		var clone3 = Instantiate(gameQuit);
		//var clone22 = Instantiate(gameOver,Vector3(21.32,0,-5),Quaternion.Euler(90,180,0));
	 	clone2.parent=mainCamera.transform;
	 	clone3.parent=mainCamera.transform;
	 	clone2.localPosition = Vector3(-5,-3,15);
	 	clone3.localPosition = Vector3(5,-3,15);
	}
};

function removeGameOverLabel(){
	var gameOverLabel = GameObject.FindGameObjectsWithTag('gameOverLabel');
	if(gameOverLabel.Length>0){
		for(var x in gameOverLabel){
			 Destroy (x);
		}
	}
}


function applyIphoneSwipeControl(moveSpeed:float){
	var fingerCount = 0;
	var gameOverLabel = GameObject.FindGameObjectsWithTag('gameOverLabel');
    for (var touch : Touch in Input.touches) {
        if (touch.phase != TouchPhase.Ended && touch.phase != TouchPhase.Canceled&&gameOverLabel.Length==0&&transform.childCount==1)
        {	
        	
        	origiVelocity = this.rigidbody.velocity;
	        transform.Translate(moveSpeed*1*Time.deltaTime*0.3*(1),0,0);
			this.transform.position.z = oldPosition.z; 
			this.rigidbody.AddForce (Vector3.right * moveSpeed * touch.deltaPosition.x);
			//if(touch.deltaPosition.x<0)this.rigidbody.AddForce (Vector3.right * moveSpeed * touch.deltaPosition.x);
			this.rigidbody.AddForce (Vector3.up * moveSpeed * touch.deltaPosition.y);		
			if(this.rigidbody.velocity.x<0){this.rigidbody.velocity.x = 0;}
		}else if(transform.childCount>1){
			//this.rigidbody.velocity = origiVelocity/4;
		}
    }
}